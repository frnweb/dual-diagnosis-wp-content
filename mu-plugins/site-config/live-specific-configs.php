<?php
// List Development Plugins
  $plugins = array(
          'wp-reroute-email/wp-reroute-email.php','debug-bar/debug-bar.php','developer/developer.php'
      );

// Live-specific configs
  if ( in_array( $_ENV['PANTHEON_ENVIRONMENT'], array( 'live' ) ) ) {

    // Disable Development Plugins
      require_once(ABSPATH . 'wp-admin/includes/plugin.php');
      foreach ($plugins as $plugin);
          if(is_plugin_active($plugin));
              deactivate_plugins($plugin);

    // Disable jetpack_development_mode
      add_filter( 'jetpack_development_mode', '__return_false' );
          }

// Configs for All environments but Live
  else {
    //
    // Activate Development Plugins
    //
      require_once(ABSPATH . 'wp-admin/includes/plugin.php');
          foreach ($plugins as $plugin);
          if(is_plugin_active($plugin))false;
              activate_plugin($plugin);
    // Enable development mode for jetpack
      add_filter( 'jetpack_development_mode', '__return_true' );

}
