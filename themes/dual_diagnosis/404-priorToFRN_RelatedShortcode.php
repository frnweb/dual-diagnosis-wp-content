<?php get_header(); 

///////
//Prepare the keyword search by pulling words from the URL

//$s = str_replace(get_site_url(),"",$wp_query->query_vars['name']);  //keeping here just in case
$s = str_replace("/","",$_SERVER['REQUEST_URI']);
//$s = trim(preg_replace("/(.*)-html|htm|php|asp|aspx)$/","$1",$s));


	$s = trim(str_replace("-"," ",$s));
	$s = urldecode($s);
	$s = strtolower(preg_replace('/[0-9]+/', '', $s )); //remove all numbers
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		//for testing:
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	




	///////
	//Prepare the list of search results

	//future option: checking if only one page returned, then immediately forwarding the person to that page instead
	//check if relevanssi plugin is activated
	if (function_exists('relevanssi_do_query')) {
		$url_terms_search = new WP_Query();
		$url_terms_search->query_vars['s']				=$s;
		$url_terms_search->query_vars['posts_per_page']	=8;
		$url_terms_search->query_vars['paged']			=0;
		$url_terms_search->query_vars['post_status']	='publish';
		relevanssi_do_query($url_terms_search);
     }
     else {
	    //global $wpdb;
		$url_terms_search = new WP_Query( array( 
			's' => 'treatment', 
			//'page_id' => 26,
			'post_type' => 'any', //array( 'post', 'page' ),
			'posts_per_page' => 8,
			'post_status' => 'publish'
		));
     }



?>
<div class="left-content-block">
	<h2>Sorry, no page was found</h2>

	<br />
	<!--<p>We're sorry, but there is something wrong with the link you clicked.</p>-->

	<?php
	if ( isset($url_terms_search) ) {
	if ( $url_terms_search->have_posts() ) { ?>
	<p><b>Were you looking for one of these?</b></p>
	<ul class="frn_suggestions">
		<?php
		while ( $url_terms_search->have_posts() ) {
			$url_terms_search->the_post();
			?>
			<li>
				<a href="<?php the_permalink();?>"><?php the_title();?></a>
			</li>
			<?php 
		}
		?>
	</ul>
	<br />
	<?php
	} else { ?>
	<p>
		We're sorry, we can't find any posts that relate to that web address. 
		Take a look at it in the address bar above and see if it looks pretty normal (no percentages with numbers after them, ampersands, etc.). 
		Make corrections if not and try again. Or just search below for the topic you were looking for.
	</p>
	<?php }
	}
	    
	?>

		<p>Our admissions coordinators are available 24 hours a day. If you'd like to ask us a question or the web address is still directing to this page, feel free to <a href="/contact/">contact us</a> or start a live chat, and we'll be glad to quickly answer any questions you have (<strong><?php echo do_shortcode('[frn_phone css_style="display:inline;" ga_phone_location="Phone Clicks in Page (404 Error)"]'); ?>).
		<br /> 
		<br />
		<div style="text-align:center;width:100%;">

			<div style="width:350px;float:left;margin-left:3%;height:110px;text-align:center;">
			<?php get_search_form(); ?>
			</div>

	<!--
			<div style="width:250px;height:70px;margin-left:3%; float:left;padding-top:20px; padding-bottom:20px; white-space:nowrap;text-align:center;">
			<?php //echo do_shortcode('[lhn_inpage]'); ?>
			</div>
	-->

	</div>

	<div style="clear:both;"></div>


</div><!-- end left-content-block -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>