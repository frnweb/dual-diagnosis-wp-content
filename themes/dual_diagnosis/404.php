<?php get_header(); ?>
<div class="left-content-block">
	<h2>Sorry, no page was found</h2>

	<br />
	<!--<p>We're sorry, but there is something wrong with the link you clicked.</p>-->

	<?php echo do_shortcode('[frn_related html="404"]'); ?>
	<br />
	<p>Our admissions coordinators are available 24 hours a day. If you'd like to ask us a question or the web address is still directing to this page, feel free to <a href="/contact/">contact us</a> or start a live chat, and we'll be glad to quickly answer any questions you have <span style="white-space:nowrap;">(<?=do_shortcode('[frn_phone css_style="none" action="Phone Clicks in Page (404 Error)"]'); ?>).</span>
	<br /> 
		<div style="text-align:center;">
			<?php echo do_shortcode('[lhn_inpage]'); ?>
		</div>
	<br />

	<div style="clear:both;"></div>


</div><!-- end left-content-block -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>