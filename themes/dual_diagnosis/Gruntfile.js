/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                src: [
                    'bower_components/foundation/js/foundation.min.js',
                    'bower_components/foundation/js/foundation/foundation.equalizer.js',
                    'library/js/jquery.waypoints.min.js',
                    'library/js/jquery.mmenu.min.all.js',
                    'library/js/scripts.js'      
                ],
                dest: 'library/js/allscripts.js'
            }
        },
        uglify: {
            build: {
                src: 'library/js/allscripts.js',
                dest: 'library/js/allscripts.min.js'
            }
        },
        sass: { 
            dist: {     
                options: {
                    style: 'expanded',
                    loadPath: ['bower_components/foundation/scss']
                },
                files: {
                    'style/css/dev-style.css': 'main.scss'
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'style/css/style.min.css': [
                        'style/css/layout.css',
                        'style/css/formatting.css',
                        'style/css/jquery.mmenu.min.css',
                        'style/css/jquery.mmenu.positioning.min.css',
                        'style/css/dev-style.css'
                    ]
                }
            }
        },
        
        watch: {
            css: {
                files: ['style/css/formatting.css','style/css/layout.css', '**/*.scss'],
                tasks: ['sass', 'cssmin']
            }
        }
        
    });
    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'sass', 'cssmin', 'watch']);
};