<?php get_header(); ?>
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=IntersectionObserver,es6"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/chart-repo.css"/>


<div class="info-title-wrap">
	<div class="row info-archive-title">
		<div class="small-12 medium-6 columns">
			<h1>Mental Health & Addiction Resource Hub</h1>
			<p>The Mental Health & Addiction Resource Hub is a resource created for you to be able to search and filter through our database of mental health and substance abuse charts and infographics. Once you find an infographic or snippet that you like, you can share or embed it on your own site, article, etc. If you don't find what you're looking for, check back soon as we will be making updates regularly. To get started, try searching a topic below!</p>
		</div>

		<div class="small-12 medium-6 columns info-svg">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 308 262.67" style="max-width: 315px;height: 268px;"><defs><mask id="0376aae1-1b19-41a1-b09e-0c1e6757e321" x="65.68" y="7.3" width="233.44" height="128.06" maskUnits="userSpaceOnUse"><rect x="66.7" y="7.3" width="231.6" height="128.06" rx="2" ry="2" style="fill:#fff"/></mask><image id="6fe861aa-d97a-4666-9300-b8d961fdb4e6" width="261" height="185" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQUAAAC5CAYAAAAh6SorAAAACXBIWXMAAAsSAAALEgHS3X78AAAQIklEQVR4Xu3bW6hndRmH8Xc8zHieUVNHSRlHMSRyCHEQ0RyoC5HUEbpQAtsJgYTEECYEAw0UmjeFCEaiXoUIUgp2EUi0vfFCPOGFDhkxiEVFRWVUdrL36bdWe71r7/9p7xkb8Qk+dzPutYPny28dJt59992QpN7MPyDpg2XmH5gl/7dJ0tFjVrOzzPwDC47AMZL+Lw7bSMz8A1PGYHhBx67hOElH1FrdTRyKWY0vNAqxegjG4R8/sHmCLZI2ZFJbw/7Gg1EGYlbrM0ch6tKMx6AfgP6CT0gndk4aOFnSYTXsq2+O/obj0Q/EcBzmOjXMMwjD08FwDPoR4MJOSaem09LWzraR0yVtyLipvjW6oz867IeCPsfjMNepYdYoDAdhPAYndxfCRXHBZ6YPpbPS2emctH3kXEnrMm6JvuiM3uiO/uiQHumSPsfjUIZhoVGI1bcM/Ae3RB2Dbd2FnN1d5Hnpw+mCtCNdmHYOXCRpQ4Y90deOaL3RHf3RIT3SJX0Ox4F+h8Mw8bSw6CBwPGGJzuh+OBdyfrQLvDh9JF2aPpo+li7r7JJ0WPRN0Red0Rvd0R8d0iNd0iedbo3W7dzDMO22gb/IvcjmqIPACm3vfjiLdUl3cVzw5Wl3ujJdla5O13Q+IWlD+pboir7ojN7ojv7okB7pkj7plF6Hw0DPw2cM00ch1j4lcE9ycqwMAvc3F0RbJi7i492FcaF70qfSden69Ol0Q7ox3SRpQ+iInuiKvuiM3vZE648O6ZEu6ZNO6bUfBjqm56mnhUmjMD4lcG/CUWR794NYI44vV3QX88loF7k3fSbdkj6bbkufS0vp8+l2SetCP0vReqIr+qIzeqM7+qNDeqRL+qRTeqVb+qXj8WlhrlE4JlafEnhowT0KR5KLux/IKl0bba1uTrdGu+AvpC+mL6V96cvprvSVkbslTTVuho7oia7oi87oje7ojw7pkS53R+uUXun27Ggdr3lamGcUxqcEjh88vNgZ7WhyRfeDWSeWiuW6I9rF8svsTwfS19M96d70zXSfpHWhHzqiJ7o6EK0zeqM7+qNDeqRL+qRTeqVb+qXj8Wlh8ihEfZ7AH+ZJJR9CcC/CyrA2HEe4Z+GIcl13AUvpzmgX97VoF/6t9EB6MH03PZQe7jwiaSF9O3RET3RFX3RGb3RHf3S4FK1L+qRTeqXb86N1TM90Td//G4UYDMOk5wn9a0ieWPJBBPckvO5gdTiWcO/CUeW27kK+mr6Rvp2+kx5N30uPpyfS99MP0pMDT0maatgL/dARPdEVfdEZvdEd/dEhPdIlfdIpvdIt/dIxPdP18PXk3KPAfcfw1oF7k13R1ofjCfcwHFlYKC7o/mhr9li0X+CH6UfpmfTj9JO0PPKspDUtj9APHdETXdEXndEb3dEfHdIjXdInndIr3dLv8BZi+FxhU0wZheFDRu47+KaaTyj5YooPJHgfuifa004ebuyLdnRhqbgwFuzpaBfOL/Zcej69kF5ML6WXR16RVIwboRv6oSN6oiv6ojN6ozv6o0N63Betz73ReqVb+qVjeqZr+l7zYeOkUegfMnL/wbfVvNa4NNpRhPei3LPw1JNV4p6GIwyLxQWyZlw0v8Sr6bV0MP00vZF+JmkhdEM/dERPdEVfdEZvdEd/dEiPdEmfdEqvdEu/dEzPdN0/bFxoFIYPGXdEuy/hCyoeYPB+lNch+6M97ODehqMMy8WFsm6vR/uFDqU301vpF+mXkhZCN/RDR4eidUVfdPZctO7ojw7pkS7pk07plW7pd0fUh42bY85R6F9H8pe2RfvXWBdGe+fJp5Xcp/DhBO9JD0R7CspDD+5xONKwYFzwz6P9Ir9Kv0m/Tb9Lv5e0ELqhHzqiJ7qiLzqjN7qjPzqkxwPR+qRTeqVb+qVjeqbrfhRWvZacZxR4Ysl7zsuiPbTgE0uebnLfwvtSXo9wT8PDD1aLow1LxoX/Otov9If0p/R2+rOkhdAN/dARPdEVfdEZvdEd/dEhPdIlfdIpvdIt/dIxPa97FPovGYejcE20b695iMGXVfdEe2/KaxKOMDwE4Z7nULRF4xf4Y7Rf7C/pr+lvkhZCN/RDR/REV/R1KFpvdEd/dEiPdEmfdEqvdDseBfre8Cjs6v7jN0b7QOKuaA81+KCC96c88ODp6MFo9z4cdVg2fhF+qXfS39M/JC2EbuiHjuiJruiLzuiN7uiPDumRLulzKVqvdEu/GxqFLd1f6j9cuqj7j/LPN2+K9o8z+BabTy8fjvaQg/eo3N/wlJSjDfdAHHlYOH4hfrl/dv4laS59M/RDR/REV/RFZ/RGd/RHh/RIl/RJp/RKt/RLx/RM1/RdvmpcZBTOjdWjcHv3Q+/rLoIvrpajvU99I9rTUo443AuxbizdcAz+LWkuw3GgI3qiK/qiM3qju+VoHdIjXdInnY5HgZ6P+Cg8EiujwCsSHn7wGoWnphx1uCfqTwnjQXhX0prGw9CfFuiJruiLzuiN7pajdUiP75tRGA7CrP9DJDXDYTjqR+HuWBmFp8JRkI6ERUaBDvtRoM//+yjwAcU8ozDr/wRJ1TyjQH9H5SjwjzkcBenwmjUKdOcoSB8gjoKkwlGQVDgKkgpHQVLhKEgqHAVJhaMgqXAUJBWOgqTCUZBUOAqSCkdBUuEoSCocBUmFoyCpcBQkFY6CpMJRkFQ4CpIKR0FS4ShIKhwFSYWjIKlwFCQVjoKkwlGQVDgKkgpHQVLhKEgqHAVJhaMgqXAUJBWOgqTCUZBUOAqSCkdBUuEoSCocBUmFoyCpcBQkFY6CpMJRkFQ4CpIKR0FS4ShIKhwFSYWjIKlwFCQVjoKkwlGQVDgKkgpHQVLhKEgqHAVJhaMgqXAUJBWOgqTCUZBUOAqSCkdBUuEoSCocBUmFoyCpcBQkFY6CpMJRkFQ4CpIKR0FS4ShIKhwFSYWjIKlwFCQVjoKkwlGQVDgKkgpHQVLhKEgqHAVJhaMgqXAUJBWOgqTCUZBUOAqSCkdBUuEoSCocBUmFoyCpcBQkFY6CpMJRkFQ4CpIKR0FS4ShIKhwFSYWjIKlwFCQVjoKkwlGQVDgKkgpHQVLhKEgqHAVJhaMgqXAUJBWOgqTCUZBUOAqSCkdBUuEoSCocBUmFoyCpcBQkFY6CpMJRkFQ4CpIKR0FS4ShIKhwFSYWjIKl4X4/Cy+EoSIfbrFGgu6NyFJZjvlFwGKT59c3MGoXlOApG4SuxMgpPhqMgHQmLjAId9qNAn0f9KPwz6jA4DtJkw07ohn7eN6PwcKyMwkvpjfSL9Lv0dvpr+nusjMJ4GCRN1jdDP3RET3RFX3RGb3S3HK1Dejzio7A9Vo/C57sf+s3uIn6QfpJeTD9Nb6Xfpj+lv6R3YuW0MBwHSdP1zdAPHdETXdEXndEb3dEfHdIjXdInnY5HgZ7XNQqbu7+0rfuP7Oz+o9ekG9NSuivdmx5K308/Ti+kg+nN9Jv0h2hHHdaNX4il+4ekhdAN/dARPdEVfdEZvdEd/dEhPdIlfS5F65Vu6ZeO6Zmu6ZvONzQKl3X/8RvS59KX0z3pu+mJ9Ex6Pr2WDqVfRTvi/DHaL8LC8Uv9TdJC6IZ+6Iie6Iq+DkXrje7ojw7pkS7pk07plW7pd8OjcFKsHoWr06fTbWlf+np6MD2efpSeS69Ge/jB0ebX0X4Blo0jD/dCf5a0ELqhHzqiJ7qiLzqjN7qjPzqkR7qkTzqlV7odjwJ9r3sUzkkXpo+lq9L16bPpS+lAeiB9L/0w2gcU3N+8nn4e7cJZNI463APxC/1e0kLohn7oiJ7oir7ojN7ojv7okB4PROuTTumVbumXjul54VE4NlZGYWs6O+1IH01XpuvSLemLaX/6Vno02kMOjjCsFq9IuGCW7FC0ex9+EZ6W/lLSQuiGfujoULSu6IvOnovWHf3RIT3SJX3SKb3SLf3uiNYzXfejQO9zj8KJ3V8+K12QLk2706fSZ9IXon01xUON76TH0tPRHnhwoSwYRxvueQ5Ge0r6RrRfSNL86IZ+6Iie6Iq+6Ize6I7+6JAe6ZI+6ZRe6ZZ+6Zie6Zq+FxqF47u/dFr6UPpw+ki6PO1Je6M9xOC+5Wvp29FehXBPwwWyXBxpuGgegvB0lF/ipWjrNvSKpGLcCN3QDx3RE13RF53RG93RHx3S475ofe6N1ivd0i8d0zNd0zedzxyFTbEyCiekU9OZ6bx0cbTXGldHu0+5Nd0RbZW+ke6PdmEsFkcZ7nF4+MGFs2a8R10eeVbSmpZH6IeO6Imu6IvO6I3u6I8O6ZEu6ZNO6ZVu6ZeO6Zmu6bsfhU0x5yjwYcMpsfIB04XR7ks4inwy3Rzt6ead6avRLoil4gjDvQ0PPVgwXpPw/pRf4MmBpyRNNeyFfuiInuiKvuiM3uiO/uiQHumSPumUXumWfumYnumavuceheFXjcOHjeenS9LHo60PDzC4Z1nqLoSF4ujCPQ0PO3gKyusR3pvyQcXDnUckLaRvh47oia7oi87oje7ojw6XonVJn3RKr3R7ftSHjOVrxlhrFCa8luS+Y3gLwXtOVueKdG204wkXwDJxZNkX7eL2R3stwvvSe6JdOJ9e3idpXeiHjuiJrg5E64ze6I7+6JAe6ZI+6ZRe6XZ463BiTHgdOW0Uhs8V+i8b+9MC9ya889zd/WAWiaMK9zA83OCpJ69DeE/KxfJl1V3RvsUeulvSVONm6Iie6Iq+6Ize6I7+6JAe6XJ3tE7ptT8l0DE9D58nzDUK/XOF8WnhjGj3JLzWuKT7gSwRRxTuXVinvdGWivejfDjBcnHBS9H+ccbtktaFfpai9URX9EVn9EZ39EeH9EiX9Emn9Eq39Ds+Jax6njBtFI6J1acF7kU4fpzb/SAWiKMJ9yy7u4vZE+29KGvFRfKJJd9e3xjtX2tJWj86oie6oi86o7c90fqjQ3qkS/qkU3ql260x4ZQQ00ZhjVuI4WnhlFgZBpaHI8nOaGvEReyK9j6UC+MLKj6t5EKv6XxC0ob0LdEVfdEZvdEd/dEhPdIlfdJpPwj0Oz4l/HcUVm3AhFEYnxa2RB0GjiLco5zX/XBed7BMfCBxaXdxHF8u6+ySdFj0TdEXndEb3dEfHdIjXdInnW6NlUEYvoZc85Sw5ijMMQwcQbg34aHFmd0P395dCF9MXRDtG2sucOfARZI2ZNgTfe2I1hvd0R8d0iNd0ied0uvcgzBxFEa3EcNh4OhxQtRxYIlO7y6ETyjP6i7snO4ih86VtC7jluiLzuiN7uiPDulxOAb0SrfDQVjztmHeURgPw3Gxehz4EOKU7kJO6y4K20ZOl7Qh46b61uiO/uiQHsdjMHyGMPWUMHUURsMwvJUYj8OWTj8S/VD0TpZ0WA376pujv77F8RiUW4aYMggzR2HGqaEfh34gepsn2CJpQya1Neyvb3I4BjNPBwuPwmgYxgMxHImh4yQdUWt1N+yyNDur8YVHYco4TBoKSe+diV3OanrDo7DgSEh6j81qdpaZf0DSB8vMPyDpg+U/V3PtRqTLVvUAAAAASUVORK5CYII="/></defs><title>Illustration</title><g style="isolation:isolate"><g id="226874e1-adf7-4ce5-8758-575ac8829afa" data-name="Layer 1"><use transform="translate(52 -5.67)" xlink:href="#6fe861aa-d97a-4666-9300-b8d961fdb4e6" style="opacity:0.12;mix-blend-mode:multiply"/><rect x="67" y="7.33" width="231" height="155" rx="2" ry="2" style="fill:#f4f4f4"/><rect x="66.7" y="7.3" width="231.6" height="128.06" rx="2" ry="2" style="fill:#00b9e4"/><path d="M248,63.33l-52-21-49,37-39-13L66.7,80.64V133.4a2,2,0,0,0,2,2h227.7a2,2,0,0,0,2-2V23.5Z" style="fill:#66d5ef"/><path d="M66.7,131.85v28.51a2,2,0,0,0,2,2H296.3a2,2,0,0,0,2-2V131.85Z" style="fill:#f4f4f4"/><g style="mask:url(#0376aae1-1b19-41a1-b09e-0c1e6757e321)"><polygon points="66.32 82.28 65.68 80.39 108 65.28 108.32 65.39 146.82 78.22 195.85 41.19 247.84 62.19 299 22.33 299.12 24.62 248.16 64.48 196.15 43.47 147.18 80.45 108 67.39 66.32 82.28" style="fill:#fff"/></g><circle cx="108" cy="66.33" r="3.71" style="fill:#9ee6f2"/><circle cx="147" cy="77.33" r="3.71" style="fill:#9ee6f2"/><circle cx="196" cy="42.33" r="3.71" style="fill:#9ee6f2"/><circle cx="248" cy="62.33" r="3.71" style="fill:#9ee6f2"/><rect x="74.5" y="139.33" width="212" height="15" rx="1.33" ry="1.33" style="fill:#e5e5e5"/><use transform="translate(-2 82.33)" xlink:href="#6fe861aa-d97a-4666-9300-b8d961fdb4e6" style="opacity:0.12;mix-blend-mode:multiply"/><rect x="13" y="95.33" width="231" height="155" rx="2" ry="2" style="fill:#f4f4f4"/><path d="M14.65,95.3h227.7a2,2,0,0,1,2,2V223.36a0,0,0,0,1,0,0H12.7a0,0,0,0,1,0,0V97.25A2,2,0,0,1,14.65,95.3Z" style="fill:#00b9e4"/><rect x="25" y="131.33" width="12" height="90" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="46" y="190.33" width="12" height="31" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="67" y="151.33" width="12" height="70" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="88" y="115.33" width="12" height="106" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="110" y="162.33" width="12" height="59" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="131" y="128.33" width="12" height="93" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="152" y="123.33" width="12" height="98" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="173" y="209.33" width="12" height="12" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="195" y="144.33" width="12" height="77" rx="1.04" ry="1.04" style="fill:#66d5ef"/><rect x="216" y="165.33" width="12" height="56" rx="1.04" ry="1.04" style="fill:#66d5ef"/><path d="M12.7,220.85v28.51a2,2,0,0,0,2,2H242.3a2,2,0,0,0,2-2V220.85Z" style="fill:#f4f4f4"/><rect x="20.5" y="228.33" width="159.5" height="15" rx="1.33" ry="1.33" style="fill:#e5e5e5"/><circle cx="197" cy="235.33" r="4" style="fill:#e5e5e5"/><circle cx="220" cy="235.33" r="4" style="fill:#e5e5e5"/></g></g></svg>
		</div>

	</div>
</div>

<?php 
    $args = array(
        'type'                     => 'infographics', 
        'parent'                   => '',
        'orderby'                  => 'id',
        'order'                    => 'ASC',
        'hide_empty'               => 1,
        'hierarchical'             => 1,   
        'taxonomy'                 => 'category'  
    ); ?>


<div class="info-search">
<div class="row">
	<div class="small-12 medium-6 columns filters-group">
        <label for="filters-search-input" class="filter-label">Search</label>
        <input class="textfield filter__search js-shuffle-search" type="search" id="filters-search-input" placeholder="Type Your Search Here  &#xF002">
    </div>
    <div class="small-12 medium-6 columns btn-group">
    	<button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button dropdown">Choose a Category</button><br>
    	<ul id="drop1" data-dropdown-content class="f-dropdown filter-options" aria-hidden="true">
    	<?php
	    $cats = get_categories($args);
	    foreach ($cats as $cat) {           
	        $cat_id= $cat->term_id;
	        $cat_name= $cat->name; ?>
	        <?php echo '<li class="btn btn--primary" data-group="'.$cat->name.'"><a href="#" onclick="return false">'.$cat->name.'</a></li>'; ?>    
    <?php  } ?>
	</ul>

</div>	
</div>
</div>

	<div class="row my-shuffle-container" id="grid">
<?php // WP_Query arguments
$args = array(
	'post_type' => array( 'infographics' ),
	'posts_per_page'         => '-1',
); ?>

<?php
// The Query
$query = new WP_Query( $args ); ?>

<?php

// The Main Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
		$image = get_field('infographic_image_source');
		$width = $image['sizes'][ $size . '-width' ];
		$srcset = wp_get_attachment_image_srcset( $image['ID'] );

	if ( get_field('infographic_image_source') ) { ?>
		<div class="small-12 medium-6 large-4 picture-item columns" data-groups='["<?php foreach((get_the_category()) as $category){ echo $category->cat_name . ''; }?>"]' data-title="<?php the_title() ?>">
		<a href="<?php the_permalink() ?>">
		<div class="picture-item__inner">
    	<div class="aspect aspect--16x9">
      	<div class="aspect__inner">
      	<div class="info-hover"><button>View Details ></button></div>	
		<img src="<?php echo $image['url']; ?>" alt="<?php the_field('alt_tag'); ?>" srcset="<?php echo $srcset ; ?>" />
		</div>
		</div>
		</div>
		<figcaption class="picture-item__title" data-category="<?php foreach((get_the_category()) as $category){ echo $category->cat_name . ''; }?>" data-tags="<?php $post_tags = get_the_tags(); if ( $post_tags ) {
		    foreach( $post_tags as $tag ) {
		    echo $tag->name . ', '; 
		    }
		}?>"><?php the_title() ?></figcaption>
		</a>
		</div>
	<?php }
	
	}
} else {
	// no posts found
}
echo '<div class="small-12 medium-6 large-4 columns my-sizer-element"></div>';
echo '</div>'; //end row my shuffle container
// Restore original Post Data
wp_reset_postdata(); ?>

<!-- SCROLL TO TOP BUTTON -->
<a href="#scrolltotop" class="scrollToTop"><i class="fa fa-caret-up"></i></a>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="../wp-content/themes/dual_diagnosis/node_modules/shufflejs/dist/shuffle.js"></script>
<script src="../wp-content/themes/dual_diagnosis/homepage.js"></script>
	<!-- BEGIN SCROLL TO TOP BUTTON -->
	<script type="text/javascript">
	$(document).ready(function(){
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});			
	});
	</script>
	<!--  END SCROLL TO TOP BUTTON -->
<?php get_footer(); ?>