<?php get_header(); ?>
<div class="large-8 columns">
	<div id="page-id">
		<h1>Articles & Publications</h1>
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php query_posts('post_type=resource&post_parent=0&posts_per_page=100');?>
	<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>
		<article class="resource-main">
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<p><?php the_excerpt(); ?></p>
			<a href="<?php the_permalink();?>" class="arrow-link">View Articles</a>
		</article><!-- end resource -->
	<?php endwhile; endif; ?>
	<?php wp_reset_query();?>
</div><!-- end left-content_block -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>