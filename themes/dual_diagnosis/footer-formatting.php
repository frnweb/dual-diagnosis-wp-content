<div class="wide-container">
    <footer class="large-12 columns no-space">
        <div class="row clearfix">
            <!-- logo -->
            <div class="small-12 columns text-center">
            </div>
            <!-- /logo -->
        </div>
    </footer>
</div><!-- end footer-container -->
<?php wp_footer(); ?>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/library/js/allscripts.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.equalizer.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.dropdown.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.accordion.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.reveal.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.orbit.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.js"></script>
<script src="http://alexgorbatchev.com/pub/sh/current/scripts/shCore.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/library/functions/syntaxhighlighter/shBrushPhp.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/library/functions/syntaxhighlighter/shBrushPlain.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/library/functions/syntaxhighlighter/shBrushCss.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/library/functions/syntaxhighlighter/shBrushJScript.js"></script>
<script>
    SyntaxHighlighter.all()
    $(document).foundation({
        equalizer : {
            equalize_on_stack: true
        },
        orbit: {
    animation: 'slide',
    timer_speed: 1000,
    pause_on_hover: true,
    animation_speed: 500,
    navigation_arrows: true,
    bullets: false
  }
    });
    $(document).ready(function(){
        $("#my-responsive-menu").mmenu();
        $("#responsive-menu-button").click(function() {
            $("#my-responsive-menu").trigger("open.mm");
            $("#my-responsive-menu").trigger("close.mm");
        });
        $("#my-menu").mmenu({         
            offCanvas: {
                position  : "top",
                zposition : "front"
            }
        });
        $("#more-menu-close").click(function() {
            $("#my-menu").trigger("close.mm");
        });
//        $( ".crossRotate" ).click(function() {
//            //alert($( this ).css( "transform" ));
//            if (  $( this ).css( "transform" ) == 'none' ){
//                $(this).css("transform","rotate(180deg)");
//            } else {
//                $(this).css("transform","" );
//            }
//        });
//        $('.facility-gallery-slider-block').slick({
//            dots: true,
//            arrows: true,
//            slidesToShow: 1,
//            slidesToScroll: 1,
//            useCSS: false
//        });
//        $('.gallery').featherlightGallery({
//            gallery: {
//                fadeIn: 300,
//                fadeOut: 300
//            },
//            openSpeed:    300,
//            closeSpeed:   300
//        });
 
    });
</script>
</body>
</html>
