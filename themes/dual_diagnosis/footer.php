</div><!-- end main-block -->
</div><!-- end wrapper div -->
</div><!-- end inside div -->
</div>
<footer id="footer" class="clearfix">
    <div id="footer-top">
        <div class="row">
            <div class="inside">
                <div id="company-info" class="large-3 columns">
                    <div id="footer-logo">Dual Diagnosis logo</div>
                    <p><?php echo stripslashes(get_option('frothy_about')); ?></p>
                </div><!-- end company-info -->
                <div id="link-block" class="large-9 columns">
                    <div class="medium-4 columns">
                        <?php wp_nav_menu(array("menu" => "Footer Links 1", 'container' => 'false', 'items_wrap' => '<h3>Quick Links</h3><ul class="no-bullet">%3$s</ul>')); ?>
                    </div>
                    <div class="medium-4 columns">
                        <?php wp_nav_menu(array("menu" => "Footer Links 2", 'container' => 'false', 'items_wrap' => '<h3> </h3><ul class="no-bullet">%3$s</ul>')); ?>
                    </div>
                    <div class="medium-4 columns">
                        <?php wp_nav_menu(array("menu" => "Footer Links 3", 'container' => 'false', 'items_wrap' => '<h3>General Information</h3><ul class="no-bullet">%3$s</ul>')); ?>
                    </div>
                </div><!-- end link-block -->
            </div><!-- end wrapper div -->
        </div><!-- end inside div -->
    </div><!-- end footer-top -->
    <div id="footer-bottom" class="medium-12 columns">
        <div class="row">
            <div class="inside">
                <div id="copyright" class="large-4 columns small-text-center large-text-left">&copy;<?php echo date("Y "); echo stripslashes(get_option('frothy_copyright')); ?></div>
                <div id="footer-phone" class="large-4 columns text-center"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></div>
                <div id="footer-link" class="large-4 columns small-text-center large-text-right"><a rel="nofollow" target="_blank" href="/">Foundations Recovery Network</a></div>
                <div class="frn_footer_extension">
					Supported by <strong><a rel="nofollow" target="_blank" href="https://www.foundationsrecoverynetwork.com">Foundations Recovery Network</a></strong> | 1000 Health Park Drive, Suite 400 Brentwood, TN 37027
				</div><!-- end footer extension -->
            </div>
        </div>
    </div><!-- end footer-bottom -->
</footer>

<?php wp_footer(); ?>

</div>
<?php //the following closes the DIV that's required to get LHN chat buttons to work. Mmenu rewrites all code on the page, but adding this avoids that. The ID is added to the script below. ?>
</div>

</body>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/library/js/allscripts.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/library/js/frn.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.1/js/foundation.min.js"></script>
<script>

    jQuery(function($) {
        $(document).foundation();
    $(document).ready(function(){
        $('.main-block').waypoint(function(up) {
            $( "#sub-head" ).toggleClass( "hide" );
        });
        $("#my-responsive-menu").mmenu({
          }, {
             offCanvas: {
                pageSelector: "#mmenu_wrapper"
             }
          });
        $("#responsive-menu-button").click(function() {
            $("#my-responsive-menu").trigger("open.mm");
            $("#my-responsive-menu").trigger("close.mm");
            $(this).toggleClass('open');
        });
            $("#responsive-menu-button-sub").click(function() {
            $("#my-responsive-menu-sub").trigger("open.mm");
            $("#my-responsive-menu-sub").trigger("close.mm");
            $(this).toggleClass('open-sub');
        });

    });

    });
</script>
<!-- Scroll past header messaging -->
<div id="sub-head" class="hide">
    <div class="wrapper hide-for-small-only">
        <div class="row">
            <div class="logo">Dual Diagnosis</div>
            <span class="call-text">Call to speak with a treatment admissions counselor</span>
            <div class="phone orange"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sub-Header"]'); ?></div>
        </div>
    </div>
    <div class="show-for-small-only">
        <div class="responsive-menu show-for-medium-down left">
            <a href="#my-responsive-menu-sub" id="responsive-menu-button-sub" class="resp-menu-close">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
        <div id="logo"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></div>
        <p id="tagline"><?php bloginfo( 'description' ); ?></p>
        <div id="top-bar">
            <div id="phone-bar"><span class="white-text">All Calls Are Confidential</span> <span class="phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header"]'); ?></span></div><!-- end phone-bar -->
        </div>
		
    </div>
</div><!-- end sub-head-->
</html>