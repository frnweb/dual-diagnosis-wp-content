<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
    <!--This is the Header Specifically used for the formatting page.-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php the_title(); ?></title>
        
        <!-- dns prefetch -->
        <link href="//www.google-analytics.com" rel="dns-prefetch">
        
        <!-- meta -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        
        <!-- icons -->
        <link href="<?php echo get_template_directory_uri(); ?>/style/images/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/style/images/icons/touch.png" rel="apple-touch-icon-precomposed">
        <!-- css + javascript -->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/normalize.css" />
        <link href="http://alexgorbatchev.com/pub/sh/current/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/library/functions/syntaxhighlighter/shCoreMidnight.css"/>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/style.min.css"/>
<style type="text/css">
    #content{margin-bottom: 10rem;}
    .row{max-width: 100%;}
</style>
    </head>
    <body <?php body_class(); ?>>
        <!-- header -->
        <div class="wide-container clearfix">
            <header>
                <div class="large-12 columns clearfix header-block">
                    <div class="row pad header-top-block">
                        <div class="responsive-menu show-for-small-only left crossRotate">
                            <a href="#my-responsive-menu" id="responsive-menu-button" class="resp-menu-close"></a>
                        </div>
                        <div class="medium-3 columns">
                            <!-- logo -->
                                <div id="logo"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- end header-container -->