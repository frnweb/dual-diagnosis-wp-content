<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_tag_schema(); ?><?php language_attributes(); ?> class="no-js"><!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">	
        <!-- Google Chrome Frame for IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="cf-2fa-verify" content="bc3b9eb3df09ad7">
<?php if (is_search()) { ?>
        <meta name="robots" content="noindex, nofollow" /> 
<?php } ?>
        <title itemprop="name"><?php wp_title(''); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <?php /* This is the traditional favicon.
                 - size: 16x16 or 32x32
                 - transparency is OK
                 - see wikipedia for info on browser support: http://mky.be/favicon/ 
        */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
        <?php /* The is the icon for iOS's Web Clip.
                 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4's retina display (IMHO, just go ahead and use the biggest one)
                 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
                 - Transparency is not recommended (iOS will put a black BG behind the icon) 
        */ ?>
        <style type="text/css">
            #my-responsive-menu:not(.mm-menu){display:none}
            #my-responsive-menu-sub:not(.mm-menu){display:none}
        </style>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/foundation.css"/>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/style.min.css"/>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/frn.css"/>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php //if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php //all our JS is at the bottom of the page, except for Modernizr. ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/library/js/modernizr.custom.min.js"></script>
<?php wp_head(); ?>
<?php 
global $frn_mobile;
if(is_user_logged_in() && $frn_mobile=="Smartphone") : ?>
        <style>
        html #wpadminbar {
            top: -46px;
        }
        </style>
<?php endif; ?>
<?php

$image = get_field('infographic_image_source');

if( $image ) {

	$meta_url = $image['url'];
	echo "\n".'<meta property="og:image" content="' . $meta_url . '" />';
	echo "\n".'<meta property="twitter:image" content="' . $meta_url . '" />';

}
?>
    </head>
    <body <?php body_class(); ?>><?php 
        //the following is required to get LHN chat buttons to work. Mmenu rewrites all code on the page, but adding this avoids that. This ID is added to the mmenu script in the footer. ?>
        <div id="mmenu_wrapper">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="clearfix">
            <header class="medium-12 columns">
                <div class="row">
                    <div class="responsive-menu show-for-medium-down left">
                        <a href="#my-responsive-menu" id="responsive-menu-button" class="resp-menu-close" onClick="ga('send', 'event', 'Hamburger Menu (Mobile Only)', 'Menu Opens & Closes'); return false; ">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                    <div id="logo"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></div>
                  <?php /*?>  <p id="tagline"><?php bloginfo( 'description' ); ?></p><?php */?>
                    <div id="top-bar">
                        <div id="phone-bar"><span class="white-text" style="line-height: 1.2; font-weight: normal;">All calls to Foundations Recovery<br class="hide-for-medium"> Network are confidential. Get help today</span> <span class="phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header"]'); ?></span></div><!-- end phone-bar -->
                        <nav id="topnav">
                            <ul>
                                <li><a href="<?php echo get_option('home'); ?>/about-us/">About Us</a></li>
                                <li><a href="<?php echo get_option('home'); ?>/contact-us/">Contact Us</a></li>
                            </ul>
                        </nav>
                        
                    </div><!-- end top-bar -->
                    <div id="menu" class="hide-for-medium-down">
                        <nav id="mainnav">
                        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
                        </nav><!-- end topnav div -->
                    </div><!-- end menu -->
                </div>
            </header><!-- end header -->
            <nav id="my-responsive-menu">
                    <?php wp_nav_menu( array( 'menu' => 'Main Navigation', 'menu_class' => 'responsive-nav-menu', 'depth' => 3, 'items_wrap' => '<div class="responsive-nav-header"><ul>    <li><a href="/">Home</a></li>%3$s    <li><a href="/about-us/">About Us</a></li><li><a href="/contact-us/">Contact Us</a></li></ul></div>')); ?>
            </nav>
            <nav id="my-responsive-menu-sub">
                    <?php wp_nav_menu( array( 'menu' => 'Main Navigation', 'menu_class' => 'responsive-nav-menu', 'depth' => 3, 'items_wrap' => '<div class="responsive-nav-header"><ul>    <li><a href="/">Home</a></li>%3$s    <li><a href="/about-us/">About Us</a></li><li><a href="/contact-us/">Contact Us</a></li></ul></div>')); ?>
            </nav>

            <div class="small-12 columns">
                <div class="row main-block">