<?php get_header(); ?>
    
<div id="home-promo" class="verticalmargin">
	<?php query_posts('post_type=home_promo&posts_per_page=1');?>
	<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>
	<?php wp_reset_query();?>
</div><!-- end home-promo -->
<div class="clearfix">
<div class="small-12 columns">
    <div class="cta-block valign-middle">
        <div class="medium-8 columns small-text-center medium-text-left verticalmargin">
            <h4><?php echo stripslashes(get_option('frothy_top_large_text')); ?></h4>
            <p><?php echo stripslashes(get_option('frothy_top_paragraph_text')); ?></p>
        </div><!-- end text-block -->
        <div class="medium-4 columns small-text-center medium-text-right verticalmargin">
            <a href="<?php echo get_site_url(); ?><?php echo stripslashes(get_option('frothy_top_button_url')); ?>" class="button"><?php echo stripslashes(get_option('frothy_top_button_text')); ?></a>
        </div><!-- end side-block -->
    </div><!-- end cta-block -->
</div>
</div>
<div class="clearfix">
<div class="large-8 columns">
    <div class="content-tabs">
        <div class="menu-wrapper">
            <ul class="tab-menu">
                <li class="one"><a href="#" class="current"><?php echo stripslashes(get_option('frothy_tab_1_title')); ?></a></li>
                <li class="two"><a href="#" class=""><?php echo stripslashes(get_option('frothy_tab_2_title')); ?></a></li>
<!--                <li class="three"><a href="#" class=""><?php //echo stripslashes(get_option('frothy_tab_3_title')); ?></a></li>-->
            </ul>
        </div><!-- end menu-wrapper -->
        <div class="tab-content-wrapper">
            <div class="one dynamic" >
		   <?php echo stripslashes(get_option('frothy_tab_1_content')); ?>
            </div><!-- end one dynamic -->
            <div class="two dynamic" style="display:none;">
		   <?php echo stripslashes(get_option('frothy_tab_2_content')); ?>
            </div><!-- end one dynamic -->
            <!-- <div class="three dynamic" style="display:none;">
		   <?php // echo stripslashes(get_option('frothy_tab_3_content')); ?>
            </div>end one dynamic -->
        </div><!-- end tab-content-wrapper -->
    </div><!-- end tabs -->
</div><!-- end left-content-block -->
<?php get_sidebar(); ?>
</div>    
<div class="full-content-block small-text-center medium-text-left clearfix"> 
    <div class="small-12 columns">
        <div class="panel clearfix">
            <div class="medium-4 columns">
			<?php echo stripslashes(get_option('frothy_col_1_content')); ?>
            </div><!-- end three-column-item -->
            <div class="medium-4 columns">
			<?php echo stripslashes(get_option('frothy_col_2_content')); ?>
            </div><!-- end three-column-item -->
            <div class="medium-4 columns">
			<?php echo stripslashes(get_option('frothy_col_3_content')); ?>
            </div><!-- end three-column-item -->
        </div><!-- end three-column-block -->
    </div>
</div><!-- end full-content-block -->
<div class="small-12 columns">
    <div class="cta-block valign-middle">
        <div class="medium-8 columns small-text-center medium-text-left verticalmargin">
            <h4>Of the 23 million people in need of treatment, <span class="orange">only 3 million seek it.</span></h4>
            <p>According to the <em><a target="_blank" rel="nofollow" href="http://www.samhsa.gov/data/nsduh/2k10MH_Findings/2k10MHResults.htm#4.7">National Survey on Drug Use and Health, 2010</a></em>.</p>
        </div><!-- end text-block -->
        <div class="medium-4 columns small-text-center medium-text-right">
            <div class="phone orange"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Page (Homepage Bottom Banner)"]'); ?></div>
        </div><!-- end side-block -->
    </div><!-- end cta-block -->
</div>  
    
    
    
<?php get_footer(); ?>