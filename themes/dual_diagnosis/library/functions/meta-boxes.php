<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
add_filter( 'rwmb_meta_boxes', 'ranklab_register_meta_boxes' );
 
function ranklab_register_meta_boxes( $meta_boxes )
{
$prefix = 'dualdiag_';

//On-Page Link Title MB
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'onpage-link-title-mb',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => '<h2>Onpage Link Title</h2>',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'page' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'side',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		// Array for Metabox field definitions
		array(
			'name' => 'Place title for what should appear on this page within the "h1" tag',
			'id'   => $prefix."onpage_link_title",
			'type' => 'textarea',
		),
	),
);
    return $meta_boxes;
}

?>