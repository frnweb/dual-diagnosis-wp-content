<?php
// --------------------- Home Promo --------------------------------
function post_type_home_promo() {
    $labels = array(
        'name' => __( 'Home Promo', 'resourceposttype' ),
        'singular_name' => __( 'Home Promo', 'resourceposttype' ),
        'add_new' => __( 'Add New Home Promo', 'resourceposttype' ),
        'add_new_item' => __( 'Add New Home Promo', 'resourceposttype' ),
        'edit_item' => __( 'Edit Home Promo', 'resourceposttype' ),
        'new_item' => __( 'Add New Home Promo', 'resourceposttype' ),
        'view_item' => __( 'View Home Promo', 'resourceposttype' ),
        'search_items' => __( 'Search Home Promo', 'resourceposttype' ),
        'not_found' => __( 'No Home Promo found', 'resourceposttype' ),
        'not_found_in_trash' => __( 'No Home Promo found in trash', 'resourceposttype' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'editor' ),
        'capability_type' => 'page',
        'menu_position' => 5,
    );
    register_post_type( 'home_promo', $args );
}
add_action( 'init', 'post_type_home_promo' );

// Register Infographic Custom Post Type
function custom_post_infographics() {

    $labels = array(
        'name'                  => _x( 'Infographics', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Infographic', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Infographics', 'text_domain' ),
        'name_admin_bar'        => __( 'Infographics', 'text_domain' ),
        'archives'              => __( 'Infographic Archives', 'text_domain' ),
        'attributes'            => __( 'Infographic Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Infographic:', 'text_domain' ),
        'all_items'             => __( 'All Infographic', 'text_domain' ),
        'add_new_item'          => __( 'Add New Infographic', 'text_domain' ),
        'add_new'               => __( 'Add New Infographic', 'text_domain' ),
        'new_item'              => __( 'New Infographic', 'text_domain' ),
        'edit_item'             => __( 'Edit Infographic', 'text_domain' ),
        'update_item'           => __( 'Update Infographic', 'text_domain' ),
        'view_item'             => __( 'View Infographic', 'text_domain' ),
        'view_items'            => __( 'View Infographics', 'text_domain' ),
        'search_items'          => __( 'Search Infographic', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Infographic', 'text_domain' ),
        'description'           => __( 'Infographics for Chart Repository', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'category', 'post_tag', 'related-infographics' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'infographics', $args );

}
add_action( 'init', 'custom_post_infographics', 0 );


// --------------------- Resources --------------------------------
function post_type_resource() {
    $labels = array(
        'name' => __( 'Resource', 'resourceposttype' ),
        'singular_name' => __( 'Resource', 'resourceposttype' ),
        'add_new' => __( 'Add New Resource', 'resourceposttype' ),
        'add_new_item' => __( 'Add New Resource', 'resourceposttype' ),
        'edit_item' => __( 'Edit Resource', 'resourceposttype' ),
        'new_item' => __( 'Add New Resource', 'resourceposttype' ),
        'view_item' => __( 'View Resource', 'resourceposttype' ),
        'search_items' => __( 'Search Resources', 'resourceposttype' ),
        'not_found' => __( 'No Resource found', 'resourceposttype' ),
        'not_found_in_trash' => __( 'No Resource found in trash', 'resourceposttype' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' ),
        'capability_type' => 'page',
        'rewrite' => array("slug" => "resource"), // Permalinks format
        'menu_position' => 5,
        'hierarchical' => true,
        'has_archive' => true
    );
    register_post_type( 'resource', $args );
}
add_action( 'init', 'post_type_resource' );

// --------------------- Research --------------------------------
function post_type_research() {
    $labels = array(
        'name' => __( 'Research', 'researchposttype' ),
        'singular_name' => __( 'Research', 'researchposttype' ),
        'add_new' => __( 'Add New Research', 'researchposttype' ),
        'add_new_item' => __( 'Add New Research', 'researchposttype' ),
        'edit_item' => __( 'Edit Research', 'researchposttype' ),
        'new_item' => __( 'Add New Research', 'researchposttype' ),
        'view_item' => __( 'View Research', 'researchposttype' ),
        'search_items' => __( 'Search Research', 'researchposttype' ),
        'not_found' => __( 'No Research found', 'researchposttype' ),
        'not_found_in_trash' => __( 'No Research found in trash', 'researchposttype' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' ),
        'capability_type' => 'page',
        'rewrite' => array("slug" => "research"), // Permalinks format
        'menu_position' => 5,
        'hierarchical' => true,
        'has_archive' => true
    );
    register_post_type( 'research', $args );
}
add_action( 'init', 'post_type_research' );

// --------------------- Events --------------------------------
function post_type_events() {
    $labels = array(
        'name' => __( 'Events', 'researchposttype' ),
        'singular_name' => __( 'Event', 'researchposttype' ),
        'add_new' => __( 'Add New Event', 'researchposttype' ),
        'add_new_item' => __( 'Add New Event', 'researchposttype' ),
        'edit_item' => __( 'Edit Event', 'researchposttype' ),
        'new_item' => __( 'Add New Event', 'researchposttype' ),
        'view_item' => __( 'View Event', 'researchposttype' ),
        'search_items' => __( 'Search Events', 'researchposttype' ),
        'not_found' => __( 'No Event found', 'researchposttype' ),
        'not_found_in_trash' => __( 'No Event found in trash', 'researchposttype' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' ),
        'capability_type' => 'page',
        'rewrite' => array("slug" => "events"), // Permalinks format
        'menu_position' => 5,
        'hierarchical' => true,
        'has_archive' => true
    );
    register_post_type( 'events', $args );
}
add_action( 'init', 'post_type_events' );
?>