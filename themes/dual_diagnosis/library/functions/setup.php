<?php

// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// This theme styles the visual editor with editor-style.css to match the theme style.
add_editor_style();

// This theme uses post thumbnails
add_theme_support( 'post-thumbnails' );

// This theme uses wp_nav_menu()
add_theme_support( 'nav-menus' );

// This theme uses wp_nav_menu() in one location.
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
		)
	);
}
add_action( 'init', 'register_my_menus' );

// Add default posts and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );

//Disable Autosave
function disableAutoSave(){
    wp_deregister_script('autosave');
}
add_action( 'wp_print_scripts', 'disableAutoSave' );


/****************START ADMIN*********************/

//Dashboard Logo
if ( ! function_exists( 'sm_add_adminbar_site_icon' ) ) {
// add to admin area, inside head
add_action( 'admin_head', 'sm_add_adminbar_site_icon' );
// add to frontend, inside head
add_action( 'wp_head', 'sm_add_adminbar_site_icon' );
function sm_add_adminbar_site_icon() {
if ( ! is_admin_bar_showing() ) {
return;
}
echo '<style>
#wp-admin-bar-site-name > a.ab-item:before {
float: left;
width: 18px;
height: 18px;
margin: 5px 5px 0 -1px;
display: block;
content: "";
opacity: 0.4;
background:url('.get_bloginfo('template_directory').'/style/images/wp_dashboard_logo.png) !important;
}
#wp-admin-bar-site-name:hover > a.ab-item:before {
opacity: 1;
}
</style>';
}
}


/************* CUSTOM LOGIN PAGE *****************/
// calling your own login css so you can style it
function frothy_login_css() {
	/* I couldn't get wp_enqueue_style to work :( */
	echo '<link rel="stylesheet" href="' . get_stylesheet_directory_uri() . '/style/css/login.css">';
}

// changing the logo link from wordpress.org to your site
function frothy_login_url() {  return home_url(); }

// changing the alt text on the logo to show your site name
function frothy_login_title() { return get_option('blogname'); }

// calling it only on the login page
add_action('login_head', 'frothy_login_css');
add_filter('login_headerurl', 'frothy_login_url');
add_filter('login_headertitle', 'frothy_login_title');


//Change Footer Text
function modify_footer_admin () {
  echo 'Created by <a href="http://www.ranklab.com">Ranklab</a>';
}
//add_filter('admin_footer_text', 'modify_footer_admin');


//Remove Dashboard Widgets
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );


//Unregister Widgets
// unregister all default WP Widgets
function unregister_default_wp_widgets() {
	unregister_widget('WP_Widget_Pages');
	//unregister_widget('WP_Widget_Search');
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Tag_Cloud');
}
add_action('widgets_init', 'unregister_default_wp_widgets', 1);


//Remove menu items
function remove_menu_items() {
  global $menu;
  $restricted = array(__('Links'));  //, __('Posts')  //Posts aren't actually used on the site, but with Contently, it automatically goes here first. It makes things easier if we add this back so we can convert the post.
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
      unset($menu[key($menu)]);}
    }
  }

add_action('admin_menu', 'remove_menu_items');


//Page Excerpts
add_post_type_support( 'page', 'excerpt' );
	

/****************END ADMIN*********************/
	


//Replace Excerpt Ellipsis
function replace_excerpt($content) {       return str_replace('[...]','', $content);}add_filter('the_excerpt', 'replace_excerpt');

function html_tag_schema() {
    $schema = 'http://schema.org/';

    // Is single post
    if(is_single()) {
        $type = "Article";
    }

    // Is author page
    elseif( is_author() ) {
        $type = 'ProfilePage';
    }

    // Contact form page ID
    elseif( is_page(1) )
    {
        $type = 'ContactPage';
    }

    // Is search results page
    elseif( is_search() ) {
        $type = 'SearchResultsPage';
    }

    else {
        $type = 'WebPage';
    }

    echo 'itemscope="itemscope" itemtype="' . $schema . $type . '"';
}

?>