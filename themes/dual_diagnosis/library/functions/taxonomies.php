<?php
// -------------------------------  Taxonomies ---------------------------------------


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_infographic_hierarchical_taxonomy', 0 );
 
 
function create_infographic_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Related Infographics', 'taxonomy general name' ),
    'singular_name' => _x( 'Related Infographic', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Related Infographics' ),
    'all_items' => __( 'All Related Infographics' ),
    'parent_item' => __( 'Parent Related Infographic' ),
    'parent_item_colon' => __( 'Parent Related Infographic:' ),
    'edit_item' => __( 'Edit Related Infographic' ), 
    'update_item' => __( 'Update Related Infographic' ),
    'add_new_item' => __( 'Add New Related Infographic' ),
    'new_item_name' => __( 'New Related Infographic' ),
    'menu_name' => __( 'Related Infographics' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy('related-infographics',array('infographics'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'related-infographic' ),
  ));
 
}


// Register Size Infographics Custom Taxonomy
function size_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Infographic Sizes', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Infographic Size', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Infographic Size', 'text_domain' ),
		'all_items'                  => __( 'All Infographic Sizes', 'text_domain' ),
		'parent_item'                => __( 'Parent Infographic Size', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Infographic Size:', 'text_domain' ),
		'new_item_name'              => __( 'New Infographic Size', 'text_domain' ),
		'add_new_item'               => __( 'Add Infographic Size', 'text_domain' ),
		'edit_item'                  => __( 'Edit Infographic Size', 'text_domain' ),
		'update_item'                => __( 'Update Infographic Size', 'text_domain' ),
		'view_item'                  => __( 'View Infographic Size', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Infographic Sizes with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Infographic Size', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Infographic Sizes', 'text_domain' ),
		'search_items'               => __( 'Search Infographic Size', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No Infographic Sizes', 'text_domain' ),
		'items_list'                 => __( 'Infographic Sizes list', 'text_domain' ),
		'items_list_navigation'      => __( 'Infographic Size list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'size_infographic_taxonomy', array( 'infographics' ), $args );

}
add_action( 'init', 'size_taxonomy', 0 );



//Section
register_taxonomy(  
	'feature',  
	array('page'),  
	array(  
	 'hierarchical' => true,  
	 'label' => 'Feature Options',  
	 'query_var' => true,  
	 'rewrite' => true  
	)  
); 

?>