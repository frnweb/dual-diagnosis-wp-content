<?php
    
/**
 * Add function to widgets_init that'll load our widget.
 * @since 0.1
 */
add_action( 'widgets_init', 'example_load_widgets' );
    
/**
 * Register our widget.
 * 'Example_Widget' is the widget class used below.
 *
 * @since 0.1
 */
function example_load_widgets() {
	register_widget( 'Treatment_Centers_Widget' );
}
    
/**
 * Example Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 * @since 0.1
 */
class Treatment_Centers_Widget extends WP_Widget {
    
	/**
	 * Widget setup.
	 */
	//function Treatment_Centers_Widget() {
		/* Widget settings. */
		//$widget_ops = array( 'classname' => 'treatment-centers', 'description' => __('Feature treatment centers', 'treatment-centers') );
                    
		/* Widget control settings. */
		//$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'treatment-centers' );
                    
		/* Create the widget. */
		//$this->WP_Widget( 'treatment-centers', __('Treatment Centers', 'treatment-centers'), $widget_ops, $control_ops );
		function __construct() {
			parent::__construct(
			 
				// Base ID of your widget
				'treatment-centers', 
				 
				// Widget name will appear in UI
				__('Treatment Centers', 'treatment-centers'), 
				 
				// Widget description
				array( 
					'classname' => 'treatment-centers',
					'description' => __('Feature treatment centers', 'treatment-centers'), 
				) 
			);
		}
	//}
            
	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );
                    
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
                    
		/* Before widget (defined by themes). */
		echo $before_widget;
                    
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;?>
                            
<div class="widget featured-centers">
				<?php query_posts('feature=treatment-centers&posts_per_page=1&orderby=rand');?>
				<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>
					<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail( array(285,140),array('class' => 'border', 'style' => 'margin: 0 auto; display: block;') );
					}
					?>
    <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
					<?php the_excerpt();?>
    <!--Adding flexbox - Hubert Chang with Responsive Theme-->
    <div class="flexbox"><a href="<?php the_permalink();?>" class="arrow-link">Details</a></div>
				<?php endwhile; endif; ?>
				<?php wp_reset_query();?>
</div><!-- end featured-centers -->
    
    
    
<?php
		/* After widget (defined by themes). */
		echo $after_widget;
	}
            
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
                    
		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
                    
		return $instance;
	}
            
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {
            
		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Featured Treatment Centers', 'treatment-centers') );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
                    
<!-- Widget Title: Text Input -->
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
    <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
</p>
    
    
	<?php
	}
}
    
?>