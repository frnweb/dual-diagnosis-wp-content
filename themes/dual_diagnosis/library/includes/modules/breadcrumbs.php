<div id="bread-wrapper">
<?php // Breadcrumb navigation
if ( wp_is_mobile() ) {	
        if(is_singular('research')) {
        	echo '<span><a href="/research/">&#8592; Back to Our Research</a></span>';
        }elseif(is_singular('resource')) {
        	echo '<span><a href="/resource/">&#8592; Back to Articles &amp; Research</a></span>';
        }elseif(is_singular('events')) {
        	echo '<span><a href="/events/">&#8592; Back to Events</a></span>';
        } 
} else {
    if (is_page() && !is_front_page() || is_single() || is_category() || is_post_type_archive()) {
        echo '<div id="breadcrumb">';
        echo '<span><a href="'.get_bloginfo('url').'" class="home">Home</a></span>';
        echo '<span class="breadcrumb-arrow"></span>';
 		
 		if(is_post_type_archive('research')){
 			echo 'Our Research';
        }elseif(is_singular('research')) {
        	echo '<span><a href="/research/">Our Research</a></span>';
        	echo '<span class="breadcrumb-arrow"></span>';
        }elseif(is_post_type_archive('resource')){
 	echo 'Articles & Research';
        }elseif(is_singular('resource')) {
        	echo '<span><a href="/resource/">Articles & Research</a></span>';
        	echo '<span class="breadcrumb-arrow"></span>';
        }elseif(is_post_type_archive('events')){
                echo 'Events';
        }elseif(is_singular('events')) {
        	echo '<span><a href="/events/">Events</a></span>';
        	echo '<span class="breadcrumb-arrow"></span>';
        }
 		
 		/*
 		if (is_single()) {
        	echo '<a href="/blog/">Blog</a>';
        	echo '<span class="breadcrumb-arrow"></span>';
        }else{
        
        }
        */

        if (is_page()) {
            $ancestors = get_post_ancestors($post);
            if ($ancestors) {
                $ancestors = array_reverse($ancestors);
                foreach ($ancestors as $crumb) {
                    echo '<span><a href="'.get_permalink($crumb).'">'.get_the_title($crumb).'</a></span>';
                    echo '<span class="breadcrumb-arrow"></span>';
                }
            }
        }
 
 
        if (is_category()) {
            $category = get_the_category();
            echo ''.$category[0]->cat_name.'';
        }
 
        // Current page
        if (is_page() || is_single()) {
            echo '<span class="current">'.get_the_title().'</span>';
        }
        echo '</div>';
    } elseif (is_front_page()) {
        // Front page
        echo '<div class="breadcrumbs">';
        echo '<a href="'.get_bloginfo('url').'">'.get_bloginfo('name').'</a>';
        echo 'Home Page';
        echo '</div>';
    }
}
?>
</div><!-- end bread-wrapper -->