<?php
if ($post->post_parent) {
$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
} else {
$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");}
if ($children) { ?>

<div id="further-reading">
<div class="tier-content-block">
<div class="brown" style="margin-bottom:0em;">
		<h2>Further Reading</h2>
	</div><!-- end title-block -->
	<div class="tan" style="margin-top:0em;">
	<div class="link-block">
		<ul>
			<?php echo $children; ?>
		</ul>
		</div>
	</div><!-- end link-block -->
</div><!-- end tier-content-block-->
</div><!-- end further-reading -->
<?php } ?>