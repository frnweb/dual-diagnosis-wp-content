<div id="toc-share" data-equalizer>
    <div class="medium-8 columns text-block" data-equalizer-watch>
		<?php echo get_post_meta($post->ID,'frothy_toc', true);?>
    </div><!-- end text-block -->
    <div id="share-block" class="medium-4 columns" data-equalizer-watch>
        <div>
            <h2>Share</h2>
			<div class="share_buttons">
				<div class="fb-like" data-send="false" data-layout="button_count" data-width="50" data-show-faces="true"></div>
				<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
				<script type="IN/Share" data-counter="right"></script>
				<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        	</div>
		</div>
    </div><!-- end share-block -->
</div><!-- end toc-share -->