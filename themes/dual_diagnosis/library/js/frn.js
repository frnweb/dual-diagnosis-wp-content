//Mailchimp Email Form scripts. The markup can be found in /parts/contetn-emailform.php
$(document).ready(function($) {

    var expanded = false;

    //Checks if dropdown is open and displays or hides accordingly
    function showCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

      console.log("show", checkboxes);
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    };

    //Just hides the dropdown if it's open
    function hideCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

        checkboxes.style.display = "none";
        expanded = false;
    };

    $(".sl_form__multiselect__field").focus(function(e) {
      var emailNumber = e.target.id.split("--").pop().toString();
      var optionDropdown = document.getElementById("sl_form__multiselect__field--" + emailNumber);
      var emailField = document.getElementById("mce-EMAIL--" + emailNumber);

      showCheckboxes(emailNumber);

      if (document.activeElement === optionDropdown){
        $(optionDropdown).blur();
      }

      $(emailField).on('focus', function() {
       hideCheckboxes(emailNumber);
      })

      var selectOptions = '#sl_form__multiselect__options--' + emailNumber + ' input:checkbox'

      console.log("logs", selectOptions);

    $(selectOptions).on("change", function(){
      var whoOptions = [];
      $(selectOptions).each(function(){
       if($(this).is(":checked")){
         whoOptions.push(this.parentNode.textContent)
         document.getElementById("sl_who-input--" + emailNumber).innerHTML = whoOptions.concat();
         document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If more than one option is selected
       if(whoOptions.length > 1){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "Multiple Selected";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If less than one option is selected, reformat to initial state
       if(whoOptions.length < 1 || whoOptions == undefined){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "I am a...";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "inherit";
       }
      })   
    });
    });
});
//END MAILCHIMP JS
