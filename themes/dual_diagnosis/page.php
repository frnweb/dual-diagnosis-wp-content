<?php get_header();
$alt_link = rwmb_meta('dualdiag_onpage_link_title');
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="content" class="large-8 columns">
	<div id="page-id">
		<h1><?php 
    //This is where you the alternate link text is swapped with Title if present
    if ($alt_link ){ echo $alt_link;
    } else { echo the_title(); }
        ?></h1>
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'large', array('class' => 'postimage') );
	}
	?>
	<?php include(TEMPLATEPATH . "/library/includes/modules/toc-share.php");?>
	<?php the_content(); ?>
	<a href="/contact-us/" class="cta-button">Contact Us</a>
	<?php include(TEMPLATEPATH . "/library/includes/modules/further-reading.php");?>
</div><!-- end left-content_block -->
<?php endwhile; endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>