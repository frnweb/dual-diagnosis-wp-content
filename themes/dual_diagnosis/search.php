<?php 
/*
Template Name: Search Page
*/

get_header(); 

global $query_string;
wp_parse_str( $query_string, $search_query );
$search = new WP_Query( $search_query );

global $wp_query;
$total_results = $wp_query->found_posts;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>
<div class="large-8 columns">
	<div id="page-id">
		<h1><?=$total_results;?> Search Results <?=($total_results>10)? "- Page ".$paged : "" ;?></h1>
		<!--<h2>
			You Searched For: <?php //ucwords(get_search_query());?>
		</h2>-->
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php //query_posts($query_string . '&showposts=100'); ?>
	<?php get_search_form(); ?><br />
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="resource-main" id="post-<?php the_ID(); ?>">

				<h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>

				<div class="entry">

					<?php the_excerpt(); ?>

				</div>
				
				<a href="<?php the_permalink();?>" class="arrow-link">Keep Reading</a>

			</article>

		<?php endwhile; ?>
	<!--
		<div class="nav-previous alignleft">« <?php //previous_posts_link( 'Older posts' ); ?></div>
		<div class="nav-next alignright"><?php //next_posts_link( 'Newer posts' ); ?> »</div>
	-->
		<div style="width">
		<?php echo paginate_links( ); ?>	
		</div>

	<?php else : ?>

		<h2>No search results found.</h2>

	<?php endif; ?>
</div><!-- end left-content_block -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>