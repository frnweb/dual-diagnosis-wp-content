<aside class="large-4 columns small-text-center large-text-left">
<?php if (is_home() || is_front_page() || $post->post_parent == '1') {?>
		
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Sidebar') ) : ?>
	<?php endif; ?>
		
<?php } elseif (is_post_type_archive('research') || is_singular('research')) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Research Sidebar') ) : ?>
	<?php endif; ?>
	
<?php } elseif (is_post_type_archive('resource') || is_singular('resource')) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Resource Sidebar') ) : ?>
	<?php endif; ?>
	
<?php } elseif (is_post_type_archive('events') || is_singular('events')) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Events Sidebar') ) : ?>
	<?php endif; ?>
	
<?php } else { ?>

	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar') ) : ?>
	<?php endif; ?>

<?php } ?>
</aside>