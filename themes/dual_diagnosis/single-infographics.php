<?php
/*

This is the infographics single page custom post type. Email Ben.Wright@frnmail.com with any questions!

*/
?>

<?php get_header(); ?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/chart-repo.css"/>

                
	 		
			 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
               
                	<?php get_template_part('parts/loop', 'pagetitle');//grabs article title ?>
                	
                <?php endwhile; endif; ?>

                <div class="medium-6 columns info-left">

                		<a class="info-back" href="/infographics"><button>Back to Search</button></a>

						<h1 class="info-title show-for-small-only"><?php echo the_title(); ?></h1>
						<h2 class="info-category show-for-small-only"><?php $categories = get_the_category();
								if ( ! empty( $categories ) ) {
								    echo '' . esc_html( $categories[0]->name ) . '';
								} ?>
						</h2>
                		<?php if( get_field('infographic_image_source') ):
                		$image = get_field('infographic_image_source');
						$width = $image['sizes'][ $size . '-width' ];
						$srcset = wp_get_attachment_image_srcset( $image['ID'] ); ?>

                			<a href="<?php echo $image['url']; ?>">
							<img src="<?php echo $image['url']; ?>" alt="<?php the_field('alt_tag'); ?>" srcset="<?php echo $srcset ; ?>" />
							</a>

						<?php endif; ?>
						
						<p class="show-for-small-only"><?php the_field('infographic_description') ?></p>

						<?php if( get_field('backlink') ): ?>
						<h6 class="info-title show-for-small-only">This image was first used on: <a href="<?php the_field('backlink'); ?>"><?php the_field('backlink_page_title'); ?></h6></a>
						<?php endif; ?>
						<?php 
						 
						// get the infographic's taxonomy terms
						 
						$custom_taxterms = wp_get_object_terms( $post->ID, array(
						        'taxonomy' => 'related-infographics'), array('fields' => 'ids') );
						// arguments
						$args = array(
						'post_type' => 'infographics',
						'post_status' => 'publish',
						'posts_per_page' => 2,
						'orderby' => 'rand',
						'tax_query' => array(
							'relation' => 'AND',
						     array(
						        'taxonomy' => 'size_infographic_taxonomy',
								'field' => 'slug',
								'terms' => 'slice',
						    ),
						    array(
						        'taxonomy' => 'related-infographics',
						        'field' => 'id',
						        'terms' => $custom_taxterms,
						    )
						),
						'post__not_in' => array ($post->ID),
						);
						$related_items = new WP_Query( $args );
						// loop over query
						if ($related_items->have_posts()) :
						echo '<h3 class="info-tags-title hide-for-small-only">Related Slices</h3>';
						//echo '<div class="small-6 columns">';
						while ( $related_items->have_posts() ) : $related_items->the_post();
							$image2 = get_field('infographic_image_source');
							$width = $image2['sizes'][ $size . '-width' ];
							$srcset = wp_get_attachment_image_srcset( $image2['ID'] ); ?>
						
							<div class="small-6 columns info-sources-images hide-for-small-only">
						    <a href="<?php the_permalink(); ?>">
	                		<?php if( get_field('infographic_image_source') ): ?>

							<img src="<?php echo $image2['url']; ?>" alt="<?php the_field('alt_tag'); ?>" srcset="<?php echo $srcset ; ?>" />
													    </a>
							</div>
						<?php endif; ?>

						<?php
						endwhile;
						echo '';
						endif;
						// Reset Post Data
						wp_reset_postdata();
						?>
						

						<?php

							// check if the repeater field has rows of data
							if( have_rows('sources-repeater') ):
								echo '<div class="small-12 columns hide-for-small-only">';
								echo '<h3 class="info-tags-title">Sources</h3>';
								echo '<div class="info-sources-links">';
							 	// loop through the rows of data
							    while ( have_rows('sources-repeater') ) : the_row(); ?>

								<?php

									$source_display = "";
									$source_url = get_sub_field('sources');

									if($source_url !== "") {
										$source_display = get_sub_field('source_titles');
										if($source_display=="") {
											$source_display = str_replace("_","-",$source_url);
										}
									}

									echo '<a href="'.$source_url.'">'.$source_display.'</a>';

								?>

							 <?php   endwhile;
							 echo '</div>';
							 echo '</div>';
							else :

							    // no rows found

							endif;

							?>

					</div><!-- end left-content_block -->

					<?php get_template_part( 'parts/loop', 'archive' ); ?>

					<div class="medium-6 columns info-right">
					<div id="infographics-content">
						<h1 class="info-title hide-for-small-only"><?php echo the_title(); ?></h1>
						<h2 class="info-category hide-for-small-only"><?php $categories = get_the_category();
								if ( ! empty( $categories ) ) {
								    echo '' . esc_html( $categories[0]->name ) . '';
								} ?>
						</h2>
						<p class="hide-for-small-only"><?php the_field('infographic_description') ?></p>
						<?php if( get_field('backlink') ): ?>
						<h6 class="info-title hide-for-small-only">This image was first used on: <a href="<?php the_field('backlink'); ?>"><?php the_field('backlink_page_title'); ?></h6></a>
						<?php endif; ?>
						<div class="info-tags">
							<h3 class="info-tags-title">Tags</h3>
							<?php
							$posttags = get_the_tags();
							if ($posttags) {
							  foreach($posttags as $tag) {
							  	echo "<p>";
							    echo $tag->name . ' ';
							    echo "</p>"; 
							  }
							}
							?>	
						</div><!-- end info-tags -->
						<div class="info-buttons">
							<div class="large-12 columns">
								<a class="button share"><span class="st_sharethis_custom">Share</span></a>
					    	<button href="#" data-dropdown="drop2" aria-controls="drop2" aria-expanded="false" class="button dropdown embed">Embed</button><br>
					    	<ul id="drop2" data-dropdown-content class="f-dropdown filter-options" aria-hidden="true">

<?php 
									////
									//OPTION 1 (Preferred): Provide Each Piece (try to keep all on one line--no line breaks)
									//NOTE: I think "ID" is provided by the image array, but ACF says to print_r() the array result to see all available variables and I'm just doing this from memory for now
									$image = get_field('infographic_image_source'); 
									$width = $image['sizes'][ $size . '-width' ];
									$srcset = wp_get_attachment_image_srcset( $image['ID'] );
									$alt = get_field('alt_tag');
									$embed_code = "";
									if( $image['caption'] ) {
										$embed_code .='<div style="width:100%; max-width: 1'.$width.'; float:left; margin:15px 0;" class="wp-caption alignnone">'; 
									}
									$embed_code .='<a href="'.$image['url'].'"><img class="size-full" src="'.$image['url'].'" alt="'.$alt.'" width="100%" srcset="'.esc_attr( $srcset ).'"></a>';
									if( $image['caption'] ) {
										$embed_code .='<p class="wp-caption-text">'.$image['caption'].'</p></div>';
									}

									?>
<textarea id="infoinfo">
<?=$embed_code; ?>
<?php if( get_field('backlink') ): ?>
View Full Infographic at: <a href="<?php the_field('backlink'); ?>"><?php the_field('backlink_page_title'); ?></a>
<?php endif; ?>
</textarea>




							<button class="btn-copy" data-clipboard-target="#infoinfo">Copy</button> <p class="copy-instructions">Use this button to copy the embed code to your clipboard and paste anywhere in your html.</p>
						</ul>
						<div data-alert class="alert-box success radius" id="success-modal">
						 Infographic successfully copied to your clipboard!
						  <a href="#" class="close">&times;</a>
						</div>
						<div data-alert class="alert-box alert radius" id="failure-modal">
						 Unable to copy to your clipboard. Please try to manually copy the Infographic embed code by using your mouse or pressing Control + C or Command + C.
						  <a href="#" class="close">&times;</a>
						</div>
							</div>
						</div><!-- end info-buttons -->
							<?php if( has_term( 'slice', 'size_infographic_taxonomy' ) ) { 
							$term = get_the_terms( $post->ID, 'related-infographics' );
							foreach ((array)$term as $key) {
								$parents = get_ancestors( $key->term_id, 'related-infographics' );
								foreach ((array)$parents as $parent) {
									$key = get_term($parent, 'related-infographics');
									$parent = $key->slug;
								}
							}
							//echo $parent;

							$args = array(
							'post_type' => 'infographics',
							'tax_query' => array(
							'relation' => 'AND',
							array(
						        'taxonomy' => 'size_infographic_taxonomy',
								'field' => 'slug',
								'terms' => 'full',
						    ),
							array(
							'taxonomy' => 'related-infographics',
							'field' => 'slug',
							'terms' => $parent,
							)
							)
							);
							//$query = new WP_Query( $args ); // this line is useless in your code
							 
							// The Query
							$the_query = new WP_Query( $args );
							//print_r($the_query); // check if it output correct results
							 
							// The Loop
							if ( $the_query->have_posts() ) {
							echo '<h3 class="info-tags-title">Full Infographic</h3>';
							echo '<div class="full-image">';
							while ( $the_query->have_posts() ) {
							$the_query->the_post();
							$image3 = get_field('infographic_image_source');
							$width = $image3['sizes'][ $size . '-width' ];
							$srcset = wp_get_attachment_image_srcset( $image3['ID'] );
							 ?>

	                		<?php if( get_field('infographic_image_source') ): ?>
	                		<div class="medium-6 columns">
							<a href="<?php the_permalink() ?>">
							<img src="<?php echo $image3['url']; ?>" alt="<?php the_field('alt_tag'); ?>" srcset="<?php echo $srcset ; ?>" />
							</a>
							<a href="<?php the_permalink() ?>">
							<div class="info-buttons full">
							<button>View Full</button>
							</div>
							</a>
							</div>

							<?php endif; ?>

							<?php }
							echo '</div>';
							} else {
							// no posts found
							}
							/* Restore original Post Data */
							wp_reset_postdata();

								 }//end has term slice size_infographic_taxonomy
							?>

							<?php 
						 
						// get the infographic's taxonomy terms
						 
						$custom_taxterms = wp_get_object_terms( $post->ID, array(
						        'taxonomy' => 'related-infographics'), array('fields' => 'ids') );
						// arguments
						$args = array(
						'post_type' => 'infographics',
						'post_status' => 'publish',
						'posts_per_page' => 2,
						'orderby' => 'rand',
						'tax_query' => array(
							'relation' => 'AND',
						     array(
						        'taxonomy' => 'size_infographic_taxonomy',
								'field' => 'slug',
								'terms' => 'slice',
						    ),
						    array(
						        'taxonomy' => 'related-infographics',
						        'field' => 'id',
						        'terms' => $custom_taxterms,
						    )
						),
						'post__not_in' => array ($post->ID),
						);
						$related_items = new WP_Query( $args );
						// loop over query
						if ($related_items->have_posts()) :
						echo '<h3 class="info-tags-title show-for-small-only">Related Slices</h3>';
						//echo '<div class="small-6 columns">';
						while ( $related_items->have_posts() ) : $related_items->the_post();
						$image4 = get_field('infographic_image_source');
						$width = $image4['sizes'][ $size . '-width' ];
						$srcset = wp_get_attachment_image_srcset( $image4['ID'] );
						?>
							<div class="small-12 medium-6 columns info-sources-images show-for-small-only">
						    <a href="<?php the_permalink(); ?>">
						    	                		<?php if( get_field('infographic_image_source') ): ?>

							<img src="<?php echo $image4['url']; ?>" srcset="<?php echo $srcset ; ?>" />
													    </a>
							</div>
						<?php endif; ?>

						<?php
						endwhile;
						echo '';
						endif;
						// Reset Post Data
						wp_reset_postdata();
						?>
						
						<p><?php echo get_field('source_title') ?></p>
						<?php

							// check if the repeater field has rows of data
							if( have_rows('sources-repeater') ):

								echo '<div class="small-12 columns show-for-small-only">';
								echo '<h3 class="info-tags-title">Sources</h3>';
								echo '<div class="info-sources-links">';

							 	// loop through the rows of data
							    while ( have_rows('sources-repeater') ) : the_row(); ?>
								<?php

									$source_display = "";
									$source_url = get_sub_field('sources');

									if($source_url !== "") {
										$source_display = get_sub_field('source_titles');
										if($source_display=="") {
											$source_display = str_replace("_","-",$source_url);
										}
									}

									echo '<a href="'.$source_url.'">'.$source_display.'</a>';

								?>


							 <?php   endwhile;
							 echo '</div>';
							 echo '</div>';
							else :

							    // no rows found

							endif;

							?>
					</div><!-- end infographics-content -->
				</div><!-- end right-content_block -->
			
			 <script src="<?php echo network_site_url(); ?>/wp-content/themes/dual_diagnosis/node_modules/clipboard/dist/clipboard.js"></script>
			 <script type="text/javascript">
				var clipboard = new Clipboard('.btn-copy');

				clipboard.on('success', function(e) {
				    console.info('Action:', e.action);
				    console.info('Text:', e.text);
				    console.info('Trigger:', e.trigger);

				    $(e.trigger).parent('.f-dropdown').hide();
				    $("#success-modal").fadeIn(400, () => {
				    	window.setTimeout(() => {
				    		$("#success-modal").fadeOut()
				    	}, 4000)
				    })

				    e.clearSelection();
				});

				clipboard.on('error', function(e) {
				    console.error('Action:', e.action);
				    console.error('Trigger:', e.trigger);

				    $(e.trigger).parent('.f-dropdown').hide();
				    $("#failure-modal").fadeIn(400, () => {
				    	window.setTimeout(() => {
				    		$("#failure-modal").fadeOut()
				    	}, 4000)
				    })

				});
			</script>	
		 <?php get_footer(); ?>