<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<div id="content" class="large-8 columns">
	<div id="page-id">
		<h1><?php the_title(); ?></h1>
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'large', array('class' => 'postimage') );
	}
	?>
	<div class="block">
		<?php the_content(); ?>
		<a href="/contact-us/" class="cta-button">Contact Us</a>
	</div><!-- end block -->
	<?php
	$current_page = $post->ID;
	$querystr = "
		SELECT * from $wpdb->posts
		WHERE post_type = 'resource'
		AND post_parent = $current_page
		ORDER BY $wpdb->posts.post_date ASC
		";
	
	$child_pages = $wpdb->get_results($querystr, OBJECT);
	if ($child_pages):
	foreach($child_pages as $post) :
	setup_postdata($post); ?>
		<article class="resource-main">
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<p><?php the_excerpt(); ?></p>
			<a href="<?php the_permalink();?>" class="arrow-link">Read Article</a>
		</article><!-- end resource -->
	<?php endforeach; ?>
	<?php else : ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
</div><!-- end left-content_block -->
<?php endwhile; endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
	
