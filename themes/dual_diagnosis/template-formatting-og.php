<?php
/*
Template Name: Formatting
*/
?>
<?php get_header(); ?>
<div class="full-content-block">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	

	
<h2>Header Blocks</h2>
<h3 class="header-block">Header Block</h3>

<code><textarea style="height:120px;">
<h3 class="header-block">Header Block</h3>
</textarea></code>


<h2>Boxes</h2>
<div class="box">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box gray">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box lblue">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<code><textarea style="height:120px;">
<div class="box">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box gray">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

<div class="box lblue">
<h3>Lorem Ipsum Dolor Sit Amit</h3>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas <a href="#">Home</a> semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

</textarea></code>



<h2>Sub Text Paragraph Option</h2>
<p class="sub-text">This is a Sub Text Paragraph habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

<p>This is a <span class="orange">Regular Paragraph</span> habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. <span class="blue">Vestibulum tortor</span> quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

<code><textarea style="height:120px;">
<p class="sub-text">Sub Text Paragraph Option</p>
<p>This is a <span class="orange">Regular Paragraph</span> habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. <span class="blue">Vestibulum tortor</span> quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</textarea></code>


<h2>Lists</h2>

<h3>Basic</h3>
<ul>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
</ul>

<h3>Arrow</h3>
<ul class="arrow">
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li><a href"">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
</ul>

<code><textarea style="height:120px; margin-top:30px;">
<ul>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
</ul>

<h3>Arrow</h3>
<ul class="arrow">
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li><a href"">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
</ul>
</textarea></code>


<h2>Buttons</h2>
<a class="button" href="#">Button</a>
<a class="button orange"href="#">Button Orange</a>
<a class="button blue" href="#">Button Blue</a>

<code><textarea style="height:120px;margin-top:30px;">
<a class="button" href="#">Button</a>
<a class="button orange"href="#">Button Orange</a>
<a class="button blue" href="#">Button Blue</a>
</textarea></code>


		
<h2>One Fourth Columns</h2>
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->

<code><textarea>
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
</textarea></code>


<h2>Three Fourth Column</h2>
<div class="three-fourth"><h3>three-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->
<div class="one-fourth"><h3>one-fourth</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-fourth -->

<code><textarea>
<div class="three-fourth">Content Goes Here</div><!-- end three-fourth -->
<div class="one-fourth">Content Goes Here</div><!-- end one-fourth -->
</textarea></code>


<h2>One Half Column</h2>
<div class="one-half"><h3>one-half</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-half -->
<div class="one-half"><h3>one-half</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-half -->

<code><textarea>
<div class="one-half">Content Goes Here</div><!-- end one-half -->
<div class="one-half">Content Goes Here</div><!-- end one-half -->
</textarea></code>



<h2>One Third</h2>
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->

<code><textarea>
<div class="one-third">Content Goes Here</div><!-- end one-third -->
<div class="one-third">Content Goes Here</div><!-- end one-third -->
<div class="one-third">Content Goes Here</div><!-- end one-third -->
</textarea></code>



<h2>Two Third</h2>
<div class="two-third"><h3>two-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.  Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end two-third -->
<div class="one-third"><h3>one-third</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p></div><!-- end one-third -->

<code><textarea>
<div class="two-third">Content Goes Here</div><!-- end two-third -->
<div class="one-third">Content Goes Here</div><!-- end one-third -->
</textarea></code>
		
		

<h2>Accordion</h2>
<div class="accordion">		
<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>

<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>
</div><!-- end accordion div -->

<code><textarea style="height:300px;">
<div class="accordion">		
<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>

<h3>Feature Title Here<span class="plus">+</span></h3>
<div>
	<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi.</p>
</div>
</div><!-- end accordion div -->
</textarea></code>
		
		
<h2>Tabs</h2>
<div class="content-tabs">
<div class="menu-wrapper">
  <ul class="tab-menu">
    <li class="one"><a href="#" class="current">Tab One</a></li>
	<li class="two"><a href="#" class="">Tab Two</a></li>
	<li class="three"><a href="#" class="">Tab Three</a></li>
  </ul>
</div><!-- end menu-wrapper -->
<div class="tab-content-wrapper">
	<div class="one dynamic" >
	   <h3>Lorem Ipsum Dolor Sit Amit</h3>
	   <p>tab one content</p>
	</div><!-- end one dynamic -->
	<div class="two dynamic" style="display:none;">
	   tab two content
	</div><!-- end one dynamic -->
	<div class="three dynamic" style="display:none;">
	   tab three content
	</div><!-- end one dynamic -->
</div><!-- end tab-content-wrapper -->
</div><!-- end tabs -->

<code><textarea style="height:350px;">
<div class="content-tabs">
<div class="menu-wrapper">
  <ul class="tab-menu">
    <li class="one"><a href="#" class="current">Tab One</a></li>
	<li class="two"><a href="#" class="">Tab Two</a></li>
	<li class="three"><a href="#" class="">Tab Three</a></li>
  </ul>
</div><!-- end menu-wrapper -->
<div class="tab-content-wrapper">
	<div class="one dynamic" >
		<h3>Lorem Ipsum Dolor Sit Amit</h3>
	   	<p>tab one content</p>
	</div><!-- end one dynamic -->
	<div class="two dynamic" style="display:none;">
	   tab two content
	</div><!-- end one dynamic -->
	<div class="three dynamic" style="display:none;">
	   tab three content
	</div><!-- end one dynamic -->
</div><!-- end tab-content-wrapper -->
</div><!-- end tabs -->
</textarea></code>
		
	
<?php endwhile; endif; ?>
</div><!-- end full-content_block -->
<?php get_footer(); ?>