<?php
/*
Template Name: Full Image
 * Currently Not Being Used as of 03/13/2015
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<div class="full-content-block">
	<div id="page-id">
		<h1><?php the_title(); ?></h1>
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'full', array('class' => 'postimage') );
	}
	?>
</div><!-- end full-content_block -->
<div class="left-content-block">
	<?php include(TEMPLATEPATH . "/library/includes/modules/toc-share.php");?>
	<?php the_content(); ?>
	<?php include(TEMPLATEPATH . "/library/includes/modules/further-reading.php");?>
</div><!-- end left-content_block -->
<?php endwhile; endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>