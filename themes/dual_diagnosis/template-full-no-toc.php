<?php
/*
Template Name: Full No TOC
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<div class="small-12 columns">
	<div id="page-id">
		<h1><?php the_title(); ?></h1>
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'full', array('class' => 'postimage') );
	}
	?>
	<?php the_content(); ?>
	<?php include(TEMPLATEPATH . "/library/includes/modules/further-reading.php");?>
</div><!-- end full-content_block -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>