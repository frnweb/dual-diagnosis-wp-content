<?php
/*
Template Name: All Infographics
*/
?>

<?php get_header(); ?>

<div class="row">
	<div class="small-12 columns filters-group">
        <label for="filters-search-input" class="filter-label">Search</label>
        <input class="textfield filter__search js-shuffle-search" type="search" id="filters-search-input">
      </div>
</div>

<?php // WP_Query arguments
$args = array(
	'post_type'              => array( 'infographics' ),
); ?>

<?php
// The Query
$query = new WP_Query( $args ); ?>
	<div class="row my-shuffle-container" id="grid">
<?php
// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

	if ( get_field('infographic_image_source') ) { ?>
		<figure class="small-12 medium-4 picture-item columns" data-groups='["animal"]' data-date-created="2016-08-12" data-title="Crocodile">
    	<div class="aspect aspect--16x9">
      	<div class="aspect__inner">
      	<a href="<?php the_permalink() ?>">
		<img src="<?php the_field('infographic_image_source') ?>" alt="" />
		</a>
		</div>
		</div>
		<figcaption class="picture-item__title"><?php the_title() ?></figcaption>
		</figure>
	<?php }
	
	}
} else {
	// no posts found
}
echo '<div class="small-12 medium-4 columns my-sizer-element"></div>';
echo '</div>'; //end row my shuffle container
// Restore original Post Data
wp_reset_postdata(); ?>	
<script src="../wp-content/themes/dual_diagnosis/node_modules/shufflejs/dist/shuffle.js"></script>
<script src="../wp-content/themes/dual_diagnosis/node_modules/shufflejs/dist/homepage.js"></script>
<?php get_footer(); ?>