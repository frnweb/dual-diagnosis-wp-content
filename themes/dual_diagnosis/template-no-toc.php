<?php
/*
Template Name: No TOC
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<div class="large-8 columns" itemprop="mainContentOfPage">
	<div id="page-id">
		<h1><?php the_title(); ?></h1>
		<?php include(TEMPLATEPATH . "/library/includes/modules/breadcrumbs.php");?>	
	</div><!-- end pageid -->
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'large', array('class' => 'postimage') );
	}
	?>
	<?php the_content(); ?>
</div><!-- end left-content_block -->

<?php endwhile; endif; ?>
<?php get_sidebar(); ?>
<?php include(TEMPLATEPATH . "/library/includes/modules/further-reading.php");?>
<?php get_footer(); ?>